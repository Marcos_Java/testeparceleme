package testeParceleme.controller;

import java.io.File;
import java.net.URI;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import testeParceleme.dao.jpa.JPAPessoa;
import testeParceleme.model.Pessoa;

@CrossOrigin
@RestController
public class PessoaController {

	@Autowired
	private JPAPessoa daoPessoa;
	
	@Autowired
	private ServletContext servletContext;

	/*
	 * M�TODO PARA INSERIR UMA PESSOA
	 */

	@RequestMapping(value = "/pessoa", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Pessoa> inserirPessoa(@RequestBody Pessoa pessoa) {
		try {
			if (!pessoa.getNome().trim().isEmpty() || !pessoa.getSobrenome().trim().isEmpty()) {
				
				daoPessoa.inserir(pessoa);
				return ResponseEntity.created(new URI("/usuario/" + pessoa.getId())).body(pessoa);	
			}else{

				return new ResponseEntity<Pessoa>(HttpStatus.BAD_REQUEST);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Pessoa>(HttpStatus.BAD_REQUEST);
		}
	}

	/*
	 * M�TODO QUE BUSCA UMA PESSOA NO SISTEMA
	 */

	@RequestMapping(value = "/pessoa/{id}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Pessoa> buscarPessoa(@PathVariable Long id) {
		try {
			Pessoa pessoaBuscada = daoPessoa.buscar(id);
			return ResponseEntity.status(HttpStatus.OK).body(pessoaBuscada);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Pessoa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * M�TODO QUE BUSCA TODAS AS PESSOAS DO SISTEMA
	 */

	@RequestMapping(value = "/pessoa/", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public @ResponseBody ResponseEntity<List<Pessoa>> buscarTodasPessoas(HttpServletRequest request) {
		try {
			List<Pessoa> pessoas = daoPessoa.buscarTodos();

			// Verificando se a lista est� vazia
			if (!pessoas.isEmpty() || pessoas.size() > 0) {
				return ResponseEntity.status(HttpStatus.OK).body(pessoas);
			} else {
				return ResponseEntity.status(HttpStatus.OK).body(null);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<List<Pessoa>>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/pessoa/", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Pessoa> alterarTarefa(@RequestBody Pessoa pessoa) {
		try {
			if (!pessoa.getNome().trim().isEmpty() || !pessoa.getSobrenome().trim().isEmpty()) {
				
				daoPessoa.alterar(pessoa);
				return ResponseEntity.status(HttpStatus.OK).body(pessoa);	
			}else{
				return new ResponseEntity<Pessoa>(HttpStatus.BAD_REQUEST);
			}

			
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Pessoa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/*
	 * M�TODO QUE DELETA ALGUMA PESSOA DO SISTEMA
	 */

	@RequestMapping(value = "/pessoa/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletarTarefa(@PathVariable Long id, HttpServletRequest request) {
		try {

			// Buscando a pessoa para deletar o objeto
			Pessoa pessoaBuscada = daoPessoa.buscar(id);

			daoPessoa.deletar(pessoaBuscada);
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			e.printStackTrace();
			return ResponseEntity.badRequest().build();
		}
	}

	/*
	 * M�TODO PARA A PESSOA LOGAR NO SISTEMA
	 */

	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<Pessoa> logar(@RequestBody Pessoa pessoa) {

		pessoa = daoPessoa.logar(pessoa);
		if (pessoa != null) {
			return ResponseEntity.ok(pessoa);
		} else {
			return new ResponseEntity<Pessoa>(HttpStatus.UNAUTHORIZED);
		}
	}
	
	/*
	 * M�TODO PARA INSER��O DE IMAGEM
	 */
	
	@RequestMapping(value = "/uploadimg/{id}", method = RequestMethod.POST)
	public ResponseEntity<Pessoa> inserirImagem(
			@RequestParam(value = "file", required = false) MultipartFile inputFileImg,
			@PathVariable Long id) {

		try {
			Pessoa pessoa = daoPessoa.buscar(id);
			
			if (inputFileImg != null && pessoa.getId() != null) {
				try {
					
					String realPath = servletContext.getRealPath("/");
					File file = new File(realPath+"/imagens");
					
					//Verificando se a pasta j� existe no servidor
					if (file.mkdirs() == false) {
						file.mkdirs();	
					}
					
					String caminhoimagem = "/foto" + id + ".jpg";

					File destinationFile = new File(file + caminhoimagem);

					System.out.println("CAMINHO "+ destinationFile);
					System.out.println("CAMINHO2222 "+ caminhoimagem);
					
					//Transferindo a imagem para o servidor
					inputFileImg.transferTo(destinationFile);

					//Setando uma parte do caminho
					pessoa.setFoto(caminhoimagem);
					daoPessoa.alterar(pessoa);
					
					return ResponseEntity.status(HttpStatus.OK).body(pessoa);

				} catch (Exception e) {
					e.printStackTrace();
					return new ResponseEntity<Pessoa>(HttpStatus.BAD_REQUEST);
				}
			} else {
				return ResponseEntity.status(HttpStatus.OK).body(pessoa);
			}
		}  catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Pessoa>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
/*
	@RequestMapping(value = "/uploadimg/{id}", method = RequestMethod.POST)
	public ResponseEntity<Pessoa> inserirEmpresa(
			@RequestParam(value = "file", required = false) MultipartFile inputFileImg, @PathVariable Long id) {

		Pessoa pessoaBuscada = daoPessoa.buscar(id);
		try {
			System.out.println("INPUTFILEEEEEEEE " + inputFileImg.getOriginalFilename());
			//File file = new File(inputFileImg.getOriginalFilename());
			// init array with file length
			//byte[] bytesArray = inputFileImg.getBytes();

			System.out.println("BYTESSSSSSSS " + inputFileImg.getBytes());
			//FileInputStream fis = new FileInputStream(file);
			//fis.read(bytesArray); // read file into bytes[]

			pessoaBuscada.setFoto(inputFileImg.getBytes());
			daoPessoa.alterar(pessoaBuscada);
			//fis.close();

			return ResponseEntity.status(HttpStatus.OK).body(pessoaBuscada);
		} catch (Exception e) {
			e.printStackTrace();

			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
		}
	}

	@RequestMapping(value = "/img/{id}", method = RequestMethod.GET)
	public ResponseEntity<BufferedImage> devolverImagem(@PathVariable Long id) {
		
		Pessoa pessoaBuscada = daoPessoa.buscar(id);
		
		try {
			System.out.println(pessoaBuscada.getFoto());
			//FileUtils.writeByteArrayToFile(new File("exemplo"), pessoaBuscada.getFoto());
			FileOutputStream fileOuputStream = new FileOutputStream("filename"); 
		    fileOuputStream.write(pessoaBuscada.getFoto());
			System.out.println(fileOuputStream.toString());
			System.out.println(fileOuputStream);
			ByteArrayInputStream bais = new ByteArrayInputStream(pessoaBuscada.getFoto());
			//System.out.println(bais.read());
			//System.out.println("ESTOU NO IO KSKSKSKSKSKSKKSKSKS "+ImageIO.read(bais));
			BufferedImage img = ImageIO.read(bais);
			//System.out.println("ESTOU AQUIIIIIII " + img);
			return ResponseEntity.status(HttpStatus.OK).body(null);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.OK).body(null);
		}
	}
	*/
}
