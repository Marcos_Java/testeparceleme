package testeParceleme.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import testeParceleme.dao.DAO;
import testeParceleme.model.Pessoa;

@Repository
public class JPAPessoa implements DAO<Pessoa> {

	@PersistenceContext
	private EntityManager manager;

	@Override
	public Pessoa buscar(Long id) {

		return manager.find(Pessoa.class, id);
	}

	@Override
	public List<Pessoa> buscarTodos() {
		TypedQuery<Pessoa> query = manager.createQuery("from Pessoa", Pessoa.class);
		return query.getResultList();
	}

	@Override
	@Transactional
	public void inserir(Pessoa obj) {
		manager.persist(obj);

	}

	@Override
	@Transactional
	public void alterar(Pessoa obj) {
		manager.merge(obj);

	}

	@Override
	@Transactional
	public void deletar(Pessoa obj) {
		manager.remove(buscar(obj.getId()));

	}

	public Pessoa logar(Pessoa pessoa) {

		try {
			TypedQuery<Pessoa> query = manager.createQuery(
					"from Pessoa where nome = :nome and id = :id", Pessoa.class);

			query.setParameter("nome", pessoa.getNome());
			query.setParameter("id", pessoa.getId());

			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}
