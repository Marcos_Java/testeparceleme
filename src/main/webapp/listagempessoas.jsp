<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pt-BR">

	<link type="text/css" href="<c:url value="arquivoscss/menu.css" />" rel="stylesheet" />
	<link type="text/css" href="<c:url value="arquivoscss/listagempessoas.css" />" rel="stylesheet" />
	
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Lista de pessoas</title>
</head>
<body>
	<div id="menu">
	<ul>
	  <li><a id="nomeUsuario"></a></li>
	  <li style="float:right"><a id="sair">Sair</a></li>
		  <li style="float:right"><a id="lista">Lista de Pessoas</a></li>
	</ul>
	</div>
	
	<div id="tabelaTarefas">
	<div id="listaTarefas">
		<table id="lista" class="table" >
			<thead>
				<tr>
					<th>Nome</th>
					<th>Sobrenome</th>
					<th>Telefone</th>
				</tr>
			</thead>
			<tbody id="corpoTabela">
					
			</tbody>
		</table>
	</div>
</div>
</body>
<script type="text/javascript">

	$(document).ready(function(){
		if (localStorage.getItem("codigo") == null) {
			$(window.location.href = "index.jsp").fadeIn('500');
		}else{
	
			 $("#nomeUsuario").text("Bem vindo "+localStorage.getItem("nome_usuario"));
			
			carregarPessoas();	
		}
	})
	
	/*
	BUSCA A PESSOA SELECIONADA
	*/

	function buscarPessoa(obj) {
		console.log(obj);
		localStorage.setItem("busca", obj.attributes["data-id"].value);
		console.log("Vou para o buscar");
    	$(window.location.href = "informacaopessoa.jsp").fadeIn('500');
	 }
	
	
	/*
	ACRESCENTA AS LINHAS DA TABELA CONFORME A QUANTIDADE PEDIDA
	*/
	function escapeHtml(unsafe) {
	    return unsafe
	         .replace(/&/g, "&amp;")
	         .replace(/</g, "&lt;")
	         .replace(/>/g, "&gt;")
	         .replace(/"/g, "&quot;")
	         .replace(/'/g, "&#039;");
	 }
	
	/*
	CARREGA A LISTA DE PESSOAS
	*/
	
	function carregarPessoas(){
		 $('#lista tbody').slideUp('fast');
		 
			var tdHTML = '';
			var trHTML = ''; 
			
			
			$.ajax({
				url : "http://localhost/testeParceleme/pessoa/",
				dataType : "json",
				method : "GET",
				contentType : "application/json",
				
				
			}).done(function(response) {
				$('#lista tbody').empty();
				//conta quantos objetos tem no array
				$.each(response,function(index) {
					//devolve as informa��es de cada um dos objetos encontrados
					//index representa a posicao do objeto... ex: Objeto na psi��o 2
					// e o atributo desejada
					console.log(response[index].nome);
					tdHTML = '<td>' + escapeHtml(response[index].nome) + '</td>' + '<td>' + response[index].sobrenome + '</td>'+'<td>' + response[index].telefone+'</td>' ;
					
					
							
					trHTML = '<tr data-id=' + response[index].id + " onclick='buscarPessoa(this)'>" + tdHTML + '</tr>';
					
					$('#lista tbody').append(trHTML).slideDown();
				});
		});
	}
	
	/*
	VOLTA PARA A LISTA DE PESSOAS 
	*/
	
	$("#lista").click(function(){
    	$(window.location.href = "listagempessoas.jsp").fadeIn('500');
	})
	
	//M�TODO SAIR
	
	$("#sair").click(function(){
		localStorage.clear();
		$(location).attr("href" , "index.jsp" );
	})
	
	/*
	BUSCA A PESSOA LOGADA
	*/
	
	$("#nomeUsuario").click(function(){
		console.log("Buscando pessoa");
		localStorage.setItem("busca", localStorage.getItem("codigo"));
		console.log("Vou para o buscar");
    	$(window.location.href = "informacaopessoa.jsp").fadeIn('500');
	})
</script>
</html>