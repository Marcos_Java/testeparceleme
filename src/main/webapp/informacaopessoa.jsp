<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang ="pt-BR">

	<link type="text/css" href="<c:url value="arquivoscss/menu.css" />" rel="stylesheet" />
	<link type="text/css" href="<c:url value="arquivoscss/informacaopessoa.css" />" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Informa��es</title>
</head>
<body>
	<div id="menu">
		<ul>
		  <li><a id="nomeUsuario"></a></li>
		  <li style="float:right"><a id="sair">Sair</a></li>
		  <li style="float:right"><a id="lista">Lista de Pessoas</a></li>
		</ul>
	</div>
	
	<div id="informacao">
		<div id="informacaoImg">
				<img id="imagemPessoa" alt="" src="" width="250" height="250" style="border-radius: 50%" >
		</div>
		<div id="informacaoTexto">
			<b><label id="nomeUsuarioBuscado"></label></b></br>
			<label id="sobrenomeUsuario"></label></br>
			<label id="telefoneUsuario"></label>
		</div>
		<div id="btns">
			<a class="btn btn-primary btn-large btn-block" id="btnAlterar" onClick="formularioalteracao()"style="float:left;">Alterar</a>
			<a class="btn btn-primary btn-large btn-block" id="btnDeletar" onClick="deletar()" style="float:right">Deletar Conta</a>
		</div>
	</div>
</body>
<script type="text/javascript">
	$(document).ready(function(){
		if (localStorage.getItem("codigo") == null) {
			$(window.location.href = "index.jsp").fadeIn('500');
		}else{
			$("#nomeUsuario").text("Bem vindo "+localStorage.getItem("nome_usuario"));
			console.log( localStorage.getItem("busca"));
			
			//Verificando se a pessoa que est� sendo buscada � a mesma logada
			if(!(localStorage.getItem("codigo") === localStorage.getItem("busca"))){
		    	document.getElementById("btns").style.display="none";
			}
		}
		buscar();
		


	});
	
	/*
	BUSCA E SETA AS INFORMA��ES DA PESSOA BUSCADA
	*/
	
	function buscar(){
		console.log("Cheguei aqui " + localStorage.getItem("busca"));
		$.ajax({
			url : "http://localhost/testeParceleme/pessoa/" + localStorage.getItem("busca"),
			type : 'GET',	
			contentType : "application/json",

		}).done(function(response, textstatus, teste) {

			console.log("Funcionou");
			$("#nomeUsuarioBuscado").text(response.nome);
			$("#sobrenomeUsuario").text(response.sobrenome);
			$("#telefoneUsuario").text(response.telefone);
			
			//Pega a imagem da pessoa no servidor
			$("#imagemPessoa").attr('src' , "http://localhost/testeParceleme/imagens/" + response.foto);


		}).fail(function(response, textstatus, teste) {
			$("#nomeUsuarioBuscado").text("Pessoa n�o encontrada");
			console.log('STATUS ' + textstatus);
			console.log(response);

		});
	}
	
	
	
	/*
	ENVIA A PESSOA PARA A PAGINA DE ALTERA��O DE INFORMA��ES
	*/
	
	function formularioalteracao(){
		$(window.location.href = "editarpessoa.jsp").fadeIn('500');
	}
	
	
	
	/*
	DELETA A CONTA DA PESSOA QUE EST� LOGADA
	*/
	function deletar(){
		$.ajax({
			        url: "http://localhost/testeParceleme/pessoa/"+localStorage.getItem("busca"),
			        dataType: "json",
			        method: "DELETE",
			        contentType: "application/json",

			}).done(function(response, textstatus, teste){
				
				localStorage.clear();
				$(location).attr("href" , "index.jsp" );
				
			    }).fail(function(response,textstatus,teste){
			        console.log("status " + teste.status)
			    })
	}
	
	
	/*
	VOLTA PARA A LISTA DE PESSOAS 
	*/
	
	$("#lista").click(function(){
    	$(window.location.href = "listagempessoas.jsp").fadeIn('500');
	})
	
	
	
	//M�TODO SAIR
	
	$("#sair").click(function(){
		localStorage.clear();
		$(location).attr("href" , "index.jsp" );
	})
	
	
	/*
	TRAZ AS INFORMA��ES DA PESSOA LOGADA
	*/
	$("#nomeUsuario").click(function(){
		console.log("Buscando pessoa");
		localStorage.setItem("busca", localStorage.getItem("codigo"));
		console.log("Vou para o buscar");
    	$(window.location.href = "informacaopessoa.jsp").fadeIn('500');
	})
</script>
</html>