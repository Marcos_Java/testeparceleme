<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pt-BR">

	<link type="text/css" href="<c:url value="arquivoscss/menu.css" />" rel="stylesheet" />
	<link type="text/css" href="<c:url value="arquivoscss/editarpessoa.css" />" rel="stylesheet" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar</title>
</head>
<body>
	<div id="menu" style="margin-top: 8px;margin-left: 8px;">
		<ul>
		  <li><a id="nomeUsuario"></a></li>
		  <li style="float:right"><a id="sair">Sair</a></li>
		  <li style="float:right"><a id="lista">Lista de Pessoas</a></li>
		</ul>
	</div>
	
	<div id="formularioTarefa">
		<form method="post" enctype="multipart/form-data">
			<input type="hidden" id="idPessoa"/>
			 <fieldset>
		        <fieldset class="grupo">
		            <div class="campo">
		                <label for="nome">Nome</label>
		                <input type="text" id="nome" autofocus style="width: 10em" required="required"/>
		            </div>
		            <div class="campo">
		                <label for="snome">Sobrenome</label>
						<input type="text" id="sobrenome" style="width: 10em" required="required"/>
		            </div>
		            <div class="campo">
		                <label for="telefone">Telefone</label>
						<input type="text" maxlength="9" id="telefone" style="width: 10em" required="required"/>
		            </div>
		        </fieldset>
				<p><input type="file" id="btnAdicionarImagem" multiple = "multiple"/></p></br>
		         
		        
			<button type="button" id="btnSalvar" onclick="alterar()" class="botao">Salvar</button>
			
			<div id="informacaoInvalida" style="display: none;">

	 			<p>Inform��es inv�lidas</p>
	 		</div>
	       </fieldset>
	
		</form>
</div>
</body>
<script type="text/javascript">

	$(document).ready(function(){
		if (localStorage.getItem("codigo") == null) {
			$(window.location.href = "index.jsp").fadeIn('500');
		}else{
			$("#nomeUsuario").text("Bem vindo "+localStorage.getItem("nome_usuario"));
			$("#telefone").mask("(99) 9999-99999");
			buscar();
		}
	})


	function buscar(){
			console.log("Cheguei aqui " + localStorage.getItem("busca"));
			$.ajax({
				url : "http://localhost/testeParceleme/pessoa/" + localStorage.getItem("busca"),
				type : 'GET',	
				contentType : "application/json",
	
			}).done(function(response, textstatus, teste) {
	
				console.log("Funcionou");
				$("#idPessoa").val(response.id);
				$("#nome").val(response.nome);
				$("#sobrenome").val(response.sobrenome);
				$("#telefone").val(response.telefone);
	
			}).fail(function(response, textstatus, teste) {

				$(window.location.href = "listagempessoas.jsp").fadeIn('500');
	
			});
		}

	function alterar(){
		var pessoaObj;
		
			pessoaObj = {
					"id": $("#idPessoa").val(),
				    "nome": $("#nome").val(),
				    "sobrenome": $("#sobrenome").val(),
				    "telefone": $("#telefone").val()
				    };
		
		console.log($("#nome").val());
		console.log($("#sobrenome").val());
		console.log($("#telefone").val());
			    $.ajax({
			        url: "http://localhost/testeParceleme/pessoa/",
			        dataType: "json",
			        method: "PUT",
			        contentType: "application/json",
			        data: JSON.stringify ( pessoaObj ),

			}).done(function(response, textstatus, teste){
				upload(response);
				
			    }).fail(function(response,textstatus,teste){
			    	$("#informacaoInvalida").css({"display":"block","color":"red"})
			        console.log("status " + teste.status)
			    })
	}
	
	
	function upload(response){
		formData = new FormData();
		inputFile = document.getElementById("btnAdicionarImagem");					

		console.log($("#btnAdicionarImagem").val());
		var cont=0;
		$(inputFile.files).each(function(){
			formData.append('file', $(this)[cont]);
			cont++;
		});
		
		console.log(JSON.stringify(formData));
		$.ajax({
			url : "http://localhost/testeParceleme/uploadimg/"+response.id,
			method : 'POST',
			data : formData,
			processData : false,
			contentType : false,
			cache : false,				

		}).done(function(response, textstatus, teste){
			
			//Enviando para a lista de pessoas caso de certo
			$(window.location.href = "listagempessoas.jsp").fadeIn('500');
	    }).fail(function(response,textstatus,teste){
	        console.log("status " + teste.status)
	    })
	}
	/*
	VOLTA PARA A LISTA DE PESSOAS 
	*/
	
	$("#lista").click(function(){
    	$(window.location.href = "listagempessoas.jsp").fadeIn('500');
	})
	//M�TODO SAIR
	
	$("#sair").click(function(){
		localStorage.clear();
		$(location).attr("href" , "index.jsp" );
	})
	
	/*
	TRAZ AS INFORMA��ES DA PESSOA LOGADA
	*/
	$("#nomeUsuario").click(function(){
		console.log("Buscando pessoa");
		localStorage.setItem("busca", localStorage.getItem("codigo"));
		console.log("Vou para o buscar");
    	$(window.location.href = "informacaopessoa.jsp").fadeIn('500');
	})
</script>
</html>