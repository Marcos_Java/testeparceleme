<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<html lang = "pt-BR">

<link type="text/css" href="<c:url value="arquivoscss/index.css" />" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
</head>
<body>
 <div id="login">
    <div class="login">
		<div class="login-screen">
			<div class="app-title">
				<h1>Login</h1>
			</div>

    	
    	<div class="login-form">
				<div class="control-group">
				<input type="text" autofocus id="nome"  placeholder="Nome" required="required"/>
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>
				
				<div class="control-group">
				<input type="password"  id="senha" placeholder="Código de acesso" required="required"/>
				<label class="login-field-icon fui-lock" for="login-pass"></label>
				</div>

				<a class="btn btn-primary btn-large btn-block" id="btnEntrar" onClick="logar()">Logar</a><br/>
				<a class="login-link" id="linkCadastrar" onclick="carregarPaginaCadastro()">Cadastrar-se</a>
			</div>
			
			
      
         	<div id="loginInvalido">

	 			<p>Usuario ou senha inválidos</p>
	 		</div>
    </div>
    </div>
    </div>



</body>

<script type="text/javascript">


	/*
		MÉTODO DE LOGIN
	*/
	function logar(){
		
		//Criando o objeto pessoa
		var pessoaObj = {
				"nome": $("#nome").val(),
				"id": $("#senha").val(),
		}
		
		 $.ajax({
	         url: "http://localhost/testeParceleme/login/",
	         dataType: "json",
	         method: "POST",
	         contentType: "application/json",
	         data: JSON.stringify ( pessoaObj )
	
		 }).done(function(response, textstatus, teste){
		     console.log("teste = " + teste.status);
		     console.log("response = " + response);
		     	 
		     //Limpando as informções armazenadas
			 localStorage.clear();
		     	 
		     //Enviando informações para a proxima pagina
			 localStorage.setItem("codigo", response.id);
			 localStorage.setItem("nome_usuario", response.nome);
				 
		     carregarProximaPagina();
	     
	     }).fail(function(response,textstatus,teste){
	    	console.log(teste.status);
	 		console.log(response.status);
	 		console.log("CODIGO = " + textstatus);
	 		console.log("codigo fail");
	 		$('#loginInvalido').css({"display":"block","color":"red"});
	 		
	     })
	}
	
	/*
		MÉTODO PARA CARREGAR A PAGINA DEPOIS QUE AS INFORMAÇÕES DO LOGIN FOREM VERIFICADAS
	*/
	
	function carregarProximaPagina(){
		$(window.location.href = "listagempessoas.jsp").fadeIn('500');
	}

	/*
	 * MÉTODO CARREGA A PAGINA DE CADASTRO
	 */
	
	function carregarPaginaCadastro(){
		$(window.location.href = "cadastro.jsp").fadeIn('500');
	}
</script>
</html>
