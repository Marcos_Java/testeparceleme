<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="pt-BR">
		<link type="text/css" href="<c:url value="arquivoscss/cadastro.css" />" rel="stylesheet" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
        
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cadastro</title>
</head>
<body>
<div id="login">
    <div class="login">
		<div class="login-screen">
			<div class="app-title">
				<h1 id="titulo">Cadastro</h1>
			</div>

    	
    	<div class="login-form">
				<div class="control-group">
				<input type="text" autofocus id="nome"  placeholder="Nome" required="required"/>
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>
				
				<div class="control-group">
				<input type="text" autofocus id="sobrenome"  placeholder="Sobrenome" required="required"/>
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>
				
				<div class="control-group">
				<input type="text" autofocus id="telefone"  name="telefone" placeholder="Telefone" required="required"/>
				<label class="login-field-icon fui-user" for="login-name"></label>
				</div>
				

				<a class="btn btn-primary btn-large btn-block" id="btnCadastrar" onClick="inserir()">Cadastrar</a><br/>
				<a class="login-link" id="linkCadastrar" onclick="voltarLogin()">Voltar a tela de login</a>
			</div>
			
			<div id="informacaoInvalida" style="display: none;">

	 			<p>Inform��es inv�lidas</p>
	 		</div>
						
    </div>
    </div>
    </div>

</body>

<script type="text/javascript">
$(document).ready(function(){
	$("#telefone").mask("(99) 9999-99999");
})
/*
 * M�TODO PARA A INSERIR UMA PESSOA NO SISTEMA
 */

	function inserir(){
	
		//Criando objeto que ser� enviado como json
	    var pessoaObj = {
	    "nome": $("#nome").val(),
	    "sobrenome": $("#sobrenome").val(),
	    "telefone": $("#telefone").val()
	    };
		
		//Chamando a url que inseri uma pessoa no back
	    $.ajax({
	        url: "http://localhost/testeParceleme/pessoa/",
	        dataType: "json",
	        method: "POST",
	        contentType: "application/json",
	        data: JSON.stringify ( pessoaObj )
	
	}).done(function(response, textstatus, teste){
	    console.log("teste = " + teste.status);
	    console.log("response = " + response);
	
	    $("#nome").css({"display":"none"});
	    $("#titulo").text("Cadastro concluido com sucesso seu c�digo de acesso � " + response.id);
	    $("#sobrenome").css({"display":"none"});
	    $("#telefone").css({"display":"none"});
    	document.getElementById("btnCadastrar").style.display="none";
    	
    	//Caso n�o de certo
	    }).fail(function(response,textstatus,teste){
	    	$("#informacaoInvalida").css({"display":"block","color":"red"})
	        console.log("status " + teste.status)
	    })
	}



	/*
	 * M�TODO PARA VOLTAR A TELA DE LOGIN
	 */
	function voltarLogin(){
		$(window.location.href = "index.jsp").fadeIn('500');
	}
	
	

</script>

</html>